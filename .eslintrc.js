module.exports = {
  env: {
    node: true
  },
  globals: {
    NodeJS: 'readonly'
  },
  plugins: [
    '@typescript-eslint',
    'import-newlines'
  ],
  extends: [
    'eslint:recommended',
    'plugin:@typescript-eslint/recommended'
  ],
  parserOptions: {
    parser: '@typescript-eslint/parser'
  },
  root  : true,
  rules : {
    '@typescript-eslint/ban-ts-comment' : 'off',
    'array-bracket-spacing'             : ['error', 'never'],
    'arrow-parens'                      : ['error', 'as-needed'],
    'brace-style'                       : ['error', '1tbs', { allowSingleLine: false }],
    'comma-dangle'                      : ['error', 'never'],
    'comma-spacing'                     : ['error', {
      before : false,
      after  : true
    }],
    // curly                     : ['error', 'all'],
    'import-newlines/enforce' : ['error', 1000, 1000000],
    indent                    : ['error', 2],
    'key-spacing'             : ['error', {
      multiLine: {
        beforeColon : false,
        afterColon  : true
      },
      align: {
        beforeColon : true,
        afterColon  : true,
        on          : 'colon'
      }
    }],
    'no-multi-spaces': ['error', {
      exceptions: {
        Property             : true,
        ImportDeclaration    : true,
        AssignmentExpression : true,
        VariableDeclarator   : true
      }
    }],
    'no-var'                  : 'error',
    // 'object-curly-spacing'    : ['error', 'always'],
    'object-property-newline' : ['error', {
      allowAllPropertiesOnSameLine: true
    }],
    'prefer-const' : 'error',
    'quote-props'  : ['error', 'as-needed'],
    quotes         : ['error', 'single'],
    semi           : ['error', 'always'],
    'sort-imports' : ['warn', {
      ignoreCase            : true,
      ignoreDeclarationSort : true
    }],
    'space-before-blocks'                        : ['error', 'always'],
    'space-before-function-paren'                : ['error', 'never'],
    'space-infix-ops'                            : 'error',
    '@typescript-eslint/type-annotation-spacing' : 'error',
    // '@typescript-eslint/member-ordering'         : ['warn', {
    //   default: {
    //     order: 'natural-case-insensitive'
    //   }
    // }],
    '@typescript-eslint/no-empty-interface'      : 'off',
    '@typescript-eslint/no-non-null-assertion'   : 'off',
    '@typescript-eslint/no-explicit-any'         : 'off'
  }
};
