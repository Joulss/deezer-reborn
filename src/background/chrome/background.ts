/// <reference types="web-ext-types"/>

import { ErrorSource, Message, PersistentData } from '../../types';
import { config } from '../../config';

chrome.runtime.onMessage.addListener(async(request: Message): Promise<void> => {
  try {
    if (request.message === 'getTabIds') {
      const currentTabs: chrome.tabs.Tab[] = await chrome.tabs.query({ url: config.deezerUrlMatch });
      if (currentTabs.length) {
        await chrome.runtime.sendMessage({ message: 'tabIds', value: currentTabs.map((t: chrome.tabs.Tab) => t.id) } as Message);
      }
    } else if (request.message === 'error') {
      const error: string|Error = request.value || '';
      const errorSource: string = request.errorSource || '';
      const errorFunction: string = request.errorFunction || '';
      await handleError(error, errorSource, errorFunction);
    }
  } catch (e) {
    await chrome.runtime.sendMessage({ message: 'error', value: e, errorSource: ErrorSource.BACKGROUND, errorFunction: 'onMessage' } as Message);
  }
});

const handleError = async(error: string|Error, errorSource: string, errorFunction: string): Promise<void> => {
  try {
    const now: Date = new Date();
    const displayedDate: string = `${now.getHours().toLocaleString().padStart(2, '0')}:${now.getMinutes().toLocaleString().padStart(2, '0')}:${now.getSeconds().toLocaleString().padStart(2, '0')}`;
    const errorMessage: string = `<b>${displayedDate}</b> : ${error instanceof Error ? error.message : error}`;
    chrome.storage.local.get(async(storedData: PersistentData): Promise<void> => {
      storedData.errors.push(errorMessage);
      await chrome.storage.local.set({ errors: storedData.errors });
    });
    console.error(`[${errorSource}] ${errorFunction}() - error :`, error);
  } catch (e) {
    console.error('[background] handleError() - error :', e);
  }
};
