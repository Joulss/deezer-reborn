export type Message = {
  message: string
  value?: any
  errorSource?: ErrorSource
  errorFunction?: string
}

export type AddonData = {
  coverBackgroundEnabled: boolean
  lastError: string|null
  noPlaylistsEnabled: boolean
  noSidebarEnabled: boolean
  skinEnabled: boolean
}

export type Preferences = {
  baseStylesheetEnabled: boolean
  coverBackgroundStylesheetEnabled: boolean
  noSidebarStylesheetEnabled: boolean
  noPlaylistsStylesheetEnabled: boolean
  enabledPlaylistsStylesheetEnabled: boolean
}

export type PersistentData = {
  preferences: Preferences
  errors: string[]
}

export enum StylesheetsId {
  BASE = 'baseStylesheet',
  COVERBACKGROUND = 'coverBackgroundStylesheet',
  ENABLEDPLAYLISTS = 'enabledPlaylistsStylesheet',
  NOPLAYLISTS = 'noPlaylistsStylesheet',
  NOSIDEBAR = 'noSidebarStylesheet',
  FONTS = 'fontsStylesheet'
}

export type StylesheetsHrefList = {
  [key in StylesheetsId]: string;
};

export type PageData = {
  darkTheme: boolean
  previousUrl: string|null
  preferences: Preferences
}

export enum ErrorSource {
  CONTENT = 'content',
  POPUP = 'popup',
  BACKGROUND = 'background'
}

export const getPersistedDataModel = (): PersistentData => {
  return {
    preferences: {
      baseStylesheetEnabled             : true,
      coverBackgroundStylesheetEnabled  : true,
      noSidebarStylesheetEnabled        : true,
      noPlaylistsStylesheetEnabled      : true,
      enabledPlaylistsStylesheetEnabled : false
    },
    errors: []
  };
};
