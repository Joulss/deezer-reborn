import { ErrorSource, Message, PersistentData, Preferences } from '../../types';

let preferences: Preferences = null;
let tabIds: number[];

// Registering elements

const baseStylesheetCheckbox: HTMLInputElement            = document.getElementById('enableSkin') as HTMLInputElement;
const coverBackgroundStylesheetCheckbox: HTMLInputElement = document.getElementById('enableCoverBackground') as HTMLInputElement;
const noSidebarStylesheetCheckbox: HTMLInputElement       = document.getElementById('enableSidebar') as HTMLInputElement;
const noPlaylistsStylesheetCheckbox: HTMLInputElement     = document.getElementById('enablePlaylists') as HTMLInputElement;
const errorsContainer: HTMLDivElement                     = document.getElementById('errorMessagesContainer') as HTMLDivElement;

// Setting event listener for response from background script to get tabId

browser.runtime.onMessage.addListener(async(request: Message): Promise<void> => {
  try {
    if (request.message) {
      if (request.message === 'tabIds' && request.value) {
        tabIds = request.value;
      }
    }
  } catch (e) {
    await browser.runtime.sendMessage({ message: 'error', value: e, errorSource: ErrorSource.POPUP, errorFunction: 'replaceUglyIcon' } as Message);
  }
});

// Setting event listeners on checkboxes

const setEvents = async(): Promise<void> => {
  try {
    // base stylesheet
    baseStylesheetCheckbox.addEventListener('input', async(): Promise<void> => {
      for (const tabId of tabIds) {
        await browser.tabs.sendMessage(tabId, { message: 'action', value: 'baseStylesheetEnabled' } as Message);
      }
      preferences.baseStylesheetEnabled = !preferences.baseStylesheetEnabled;
      if (preferences.baseStylesheetEnabled) {
        noSidebarStylesheetCheckbox.removeAttribute('disabled');
        coverBackgroundStylesheetCheckbox.removeAttribute('disabled');
        if (!preferences.noSidebarStylesheetEnabled) {
          noPlaylistsStylesheetCheckbox.removeAttribute('disabled');
        }
      } else {
        noSidebarStylesheetCheckbox.setAttribute('disabled', 'disabled');
        coverBackgroundStylesheetCheckbox.setAttribute('disabled', 'disabled');
        noPlaylistsStylesheetCheckbox.setAttribute('disabled', 'disabled');
      }
    });
    // cover background stylesheet
    coverBackgroundStylesheetCheckbox.addEventListener('input', async(): Promise<void> => {
      for (const tabId of tabIds) {
        await browser.tabs.sendMessage(tabId, { message: 'action', value: 'coverBackgroundStylesheetEnabled' } as Message);
      }
      preferences.coverBackgroundStylesheetEnabled = !preferences.coverBackgroundStylesheetEnabled;
    });
    // no sidebar stylesheet
    noSidebarStylesheetCheckbox.addEventListener('input', async(): Promise<void> => {
      for (const tabId of tabIds) {
        await browser.tabs.sendMessage(tabId, { message: 'action', value: 'noSidebarStylesheetEnabled' } as Message);
      }
      preferences.noSidebarStylesheetEnabled = !preferences.noSidebarStylesheetEnabled;
      if (preferences.noSidebarStylesheetEnabled) {
        noPlaylistsStylesheetCheckbox.setAttribute('disabled', 'disabled');
      } else {
        noPlaylistsStylesheetCheckbox.removeAttribute('disabled');
      }
    });
    // no playlists stylesheet
    noPlaylistsStylesheetCheckbox.addEventListener('input', async(): Promise<void> => {
      for (const tabId of tabIds) {
        await browser.tabs.sendMessage(tabId, { message: 'action', value: 'noPlaylistsStylesheetEnabled' } as Message);
      }
      preferences.noPlaylistsStylesheetEnabled = !preferences.noPlaylistsStylesheetEnabled;
    });
  } catch (e) {
    await browser.runtime.sendMessage({ message: 'error', value: e, errorSource: ErrorSource.POPUP, errorFunction: 'setEvents' } as Message);
  }
};

// Applying checkboxes state on startup

const applyPreferences = async(): Promise<void> => {
  try {
    if (preferences) {
      if (preferences.baseStylesheetEnabled) {
        baseStylesheetCheckbox.setAttribute('checked', 'checked');
      } else {
        baseStylesheetCheckbox.removeAttribute('checked');
      }
      if (preferences.coverBackgroundStylesheetEnabled) {
        coverBackgroundStylesheetCheckbox.setAttribute('checked', 'checked');
      } else {
        coverBackgroundStylesheetCheckbox.removeAttribute('checked');
      }
      if (preferences.noSidebarStylesheetEnabled) {
        noSidebarStylesheetCheckbox.removeAttribute('checked');
        noPlaylistsStylesheetCheckbox.setAttribute('disabled', 'disabled');
      } else {
        noSidebarStylesheetCheckbox.setAttribute('checked', 'checked');
        noPlaylistsStylesheetCheckbox.removeAttribute('disabled');
      }
      if (preferences.noPlaylistsStylesheetEnabled) {
        noPlaylistsStylesheetCheckbox.removeAttribute('checked');
      } else {
        noPlaylistsStylesheetCheckbox.setAttribute('checked', 'checked');
      }
    }
  } catch (e) {
    await browser.runtime.sendMessage({ message: 'error', value: e, errorSource: ErrorSource.POPUP, errorFunction: 'applyPreferences' } as Message);
  }
};

// Inserting error messages

const displayErrorMessages = async(): Promise<void> => {
  try {
    const storedData: PersistentData = await browser.storage.local.get();
    errorsContainer.childNodes.forEach((node: ChildNode) => node.remove());
    if (storedData.errors.length) {
      document.getElementById('errorsContainer').classList.remove('hidden');
      storedData.errors.reverse().forEach((error: string): void => {
        const errorContainer: HTMLDivElement = document.createElement('div');
        errorContainer.innerHTML = error;
        errorsContainer.appendChild(errorContainer);
      });
    }
  } catch (e) {
    await browser.runtime.sendMessage({ message: 'error', value: e, errorSource: ErrorSource.POPUP, errorFunction: 'displayErrorMessages' } as Message);
  }
};

// Init

document.addEventListener('DOMContentLoaded', async(): Promise<void> => {
  try {
    await browser.runtime.sendMessage({ message: 'getTabIds' });
    const storedData: PersistentData = await browser.storage.local.get();
    preferences = storedData.preferences;
    await displayErrorMessages();
    await setEvents();
    await applyPreferences();
  } catch (e) {
    await browser.runtime.sendMessage({ message: 'error', value: e, errorSource: ErrorSource.POPUP, errorFunction: 'DOMContentLoaded' } as Message);
  }
});
