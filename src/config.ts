export const config = {
  pageContentId                 : 'page_content',
  pageContentSelector           : undefined,
  coverThumbnailElementSelector : '.KVvol',
  isDarkThemeSelector           : 'chakra-ui-dark',

  leftPanelWithPlaylistsWidth : '290px',
  coverBackgroundOpacity      : '0.15',
  baseCssPath                 : 'styles/base.css',
  coverBackgroundCssPath      : 'styles/coverBackground.css',
  noSidebarCssPath            : 'styles/noSidebar.css',
  noPlaylistsCssPath          : 'styles/noPlaylists.css',
  enabledPlaylistsCssPath     : 'styles/enabledPlaylists.css',
  faviconPath                 : 'images/logo/deezer-32.png',

  deezerUrlMatch: '*://www.deezer.com/*'
};

config.pageContentSelector = `#${config.pageContentId}`;
