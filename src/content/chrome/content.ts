/// <reference types="chrome"/>

import { ErrorSource, getPersistedDataModel, Message, PageData, PersistentData, StylesheetsHrefList, StylesheetsId } from '../../types';
import { config } from '../../config';

const stylesheetsHrefs: StylesheetsHrefList = {
  baseStylesheet             : chrome.runtime.getURL('styles/base.css'),
  coverBackgroundStylesheet  : chrome.runtime.getURL('styles/coverBackground.css'),
  noSidebarStylesheet        : chrome.runtime.getURL('styles/noSidebar.css'),
  noPlaylistsStylesheet      : chrome.runtime.getURL('styles/noPlaylists.css'),
  enabledPlaylistsStylesheet : chrome.runtime.getURL('styles/enabledPlaylists.css'),
  fontsStylesheet            : chrome.runtime.getURL('styles/fonts-chrome.css')
};

const pageData: PageData = {
  darkTheme   : null,
  previousUrl : null,
  preferences : null
};

// Listeners

chrome.runtime.onMessage.addListener(async(request: Message): Promise<void> => {
  try {
    if (request.message === 'action') {
      pageData.preferences[request.value] = !pageData.preferences[request.value];
      await applyPreferences();
    }
  } catch (e) {
    await chrome.runtime.sendMessage({ message: 'error', value: e, errorSource: ErrorSource.CONTENT, errorFunction: 'onMessage' } as Message);
  }
});

// Stylesheet insertion / removal

const insertStylesheet = async(stylesheetId: StylesheetsId): Promise<void> => {
  try {
    if (stylesheetsHrefs[stylesheetId]) {
      if (!document.getElementById(stylesheetId)) {
        const link: HTMLLinkElement = document.createElement('link');
        link.id = stylesheetId;
        link.type = 'text/css';
        link.rel = 'stylesheet';
        link.href = stylesheetsHrefs[stylesheetId];
        document.body.appendChild(link);
      }
    }
  } catch (e) {
    await chrome.runtime.sendMessage({ message: 'error', value: e, errorSource: ErrorSource.CONTENT, errorFunction: 'insertStyleSheet' } as Message);
  }
};

const removeStylesheet = async(stylesheetId: StylesheetsId): Promise<void> => {
  try {
    if (document.getElementById(stylesheetId)) {
      document.getElementById(stylesheetId).remove();
    }
  } catch (e) {
    await chrome.runtime.sendMessage({ message: 'error', value: e, errorSource: ErrorSource.CONTENT, errorFunction: 'removeStyleSheet' } as Message);
  }
};

// Apply preferences

const applyPreferences = async(): Promise<void> => {
  try {
    if (pageData.preferences) {
      // Base skin
      if (pageData.preferences.baseStylesheetEnabled) {
        await insertStylesheet(StylesheetsId.FONTS);
        await insertStylesheet(StylesheetsId.BASE);
        if (pageData.preferences.noPlaylistsStylesheetEnabled) {
          await insertStylesheet(StylesheetsId.NOPLAYLISTS);
        } else {
          await insertStylesheet(StylesheetsId.ENABLEDPLAYLISTS);
        }
        if (pageData.preferences.noSidebarStylesheetEnabled) {
          await insertStylesheet(StylesheetsId.NOSIDEBAR);
        }
      } else {
        await removeStylesheet(StylesheetsId.BASE);
        await removeStylesheet(StylesheetsId.FONTS);
        await removeStylesheet(StylesheetsId.NOPLAYLISTS);
        await removeStylesheet(StylesheetsId.ENABLEDPLAYLISTS);
        await removeStylesheet(StylesheetsId.NOSIDEBAR);
        await removeCoverBackground();
      }
      // Cover background
      if (pageData.preferences.baseStylesheetEnabled && pageData.preferences.coverBackgroundStylesheetEnabled) {
        await insertStylesheet(StylesheetsId.COVERBACKGROUND);
        await insertCoverBackground();
      } else if (pageData.preferences.baseStylesheetEnabled) {
        await removeStylesheet(StylesheetsId.COVERBACKGROUND);
        await removeCoverBackground();
      }
      // No Sidebar
      if (pageData.preferences.baseStylesheetEnabled && pageData.preferences.noSidebarStylesheetEnabled) {
        await removeStylesheet(StylesheetsId.ENABLEDPLAYLISTS);
        await insertStylesheet(StylesheetsId.NOSIDEBAR);
      } else if (pageData.preferences.baseStylesheetEnabled) {
        await removeStylesheet(StylesheetsId.NOSIDEBAR);
        if (!pageData.preferences.noPlaylistsStylesheetEnabled) {
          await insertStylesheet(StylesheetsId.ENABLEDPLAYLISTS);
        }
      }
      // No Playlists
      if (pageData.preferences.baseStylesheetEnabled && !pageData.preferences.noSidebarStylesheetEnabled) {
        if (pageData.preferences.noPlaylistsStylesheetEnabled) {
          await insertStylesheet(StylesheetsId.NOPLAYLISTS);
          await removeStylesheet(StylesheetsId.ENABLEDPLAYLISTS);
        } else {
          await insertStylesheet(StylesheetsId.ENABLEDPLAYLISTS);
          await removeStylesheet(StylesheetsId.NOPLAYLISTS);
        }
      }
      await saveData();
    }
  } catch (e) {
    await chrome.runtime.sendMessage({ message: 'error', value: e, errorSource: ErrorSource.CONTENT, errorFunction: 'applyPreferences' } as Message);
  }
};

const saveData = async(): Promise<void> => {
  try {
    await chrome.storage.local.set({ preferences: pageData.preferences });
  } catch(e) {
    await chrome.runtime.sendMessage({ message: 'error', value: e, errorSource: ErrorSource.CONTENT, errorFunction: 'saveData' } as Message);
  }
};

// Cover background

const coverBackgroundFadeIn = async(): Promise<void> => {
  try {
    if (document.getElementById('coverBgContainer')) {
      document.getElementById('coverBgContainer').style.opacity = config.coverBackgroundOpacity;
    }
  } catch (e) {
    await chrome.runtime.sendMessage({ message: 'error', value: e, errorSource: ErrorSource.CONTENT, errorFunction: 'handleCoverImgLoad' } as Message);
  }
};

const createCoverBackgroundContainer = async(): Promise<void> => {
  try {
    if (!document.getElementById('coverBgContainer')) {
      const coverImg: HTMLImageElement = document.createElement('img');
      coverImg.id = 'coverImg';
      coverImg.style.width = '100%';
      coverImg.addEventListener('load', coverBackgroundFadeIn);
      const coverContainer: HTMLDivElement = document.createElement('div');
      coverContainer.id = 'coverBgContainer';
      coverContainer.style.position = 'fixed';
      coverContainer.style.top = '0';
      coverContainer.style.right = '0';
      coverContainer.style.width = '100%';
      coverContainer.style.opacity = '0';
      coverContainer.style.height = '100vh';
      coverContainer.style.zIndex = '-1';
      coverContainer.style.transition = 'opacity 0.5s';
      coverContainer.style.filter = 'blur(10px)';
      coverContainer.appendChild(coverImg);
      document.getElementById(config.pageContentId).appendChild(coverContainer);
    }
  } catch (e) {
    await chrome.runtime.sendMessage({ message: 'error', value: e, errorSource: ErrorSource.CONTENT, errorFunction: 'createCoverBackgroundContainer' } as Message);
  }
};

const removeCoverBackground = async(): Promise<void> => {
  try {
    const coverImg: HTMLImageElement = document.getElementById('coverImg') as HTMLImageElement;
    document.getElementById(config.pageContentId).style.background = 'unset';
    if (document.getElementById('coverImg')) {
      coverImg.removeEventListener('load', coverBackgroundFadeIn);
      coverImg.src = '';
    }
    if (document.getElementById('coverBgContainer')) {
      document.getElementById('coverBgContainer').style.opacity = '0';
    }
  } catch (e) {
    await chrome.runtime.sendMessage({ message: 'error', value: e, errorSource: ErrorSource.CONTENT, errorFunction: 'removeCoverBackground' } as Message);
  }
};

const insertCoverBackground = async(): Promise<void> => {
  try {
    if (window.location.href.includes('artist') || window.location.href.includes('album') || window.location.href.includes('playlist') || window.location.href.includes('show') || window.location.href.includes('smarttracklist')) {
      waitForElement(config.coverThumbnailElementSelector).then(async(image: HTMLImageElement|null): Promise<void> => {
        if (image) {
          const coverImg: HTMLImageElement = document.getElementById('coverImg') as HTMLImageElement;
          const coverImgSrc: string        = image.src.replace('500x500', '1000x1000');
          coverImg.addEventListener('load', coverBackgroundFadeIn);
          coverImg.src = coverImgSrc;
          document.getElementById(config.pageContentId).style.background = pageData.darkTheme ? '#060606' : '#fff';
        } else {
          console.error('deezer reborn error (setCoverBackground()) : unable to find cover element !');
        }
      });
    }
  } catch (e) {
    await chrome.runtime.sendMessage({ message: 'error', value: e, errorSource: ErrorSource.CONTENT, errorFunction: 'setCoverBackground' } as Message);
  }
};

// Ugly icon

export const replaceUglyIcon = async(): Promise<void> => {
  try {
    const favicons: NodeListOf<Element> = document.querySelectorAll('link[rel="icon"], link[rel="shortcut icon"], link[rel="apple-touch-icon"]');
    favicons.forEach((icon: Element): void => {
      icon.remove();
    });
    const favicon: HTMLLinkElement = document.createElement('link');
    favicon.rel = 'icon';
    favicon.href = chrome.runtime.getURL(config.faviconPath);
    document.querySelector('head').appendChild(favicon);
  } catch (e) {
    await chrome.runtime.sendMessage({ message: 'error', value: e, errorSource: ErrorSource.CONTENT, errorFunction: 'replaceUglyIcon' } as Message);
  }
};

// Utils

const waitForElement = async(selector: string): Promise<Element|null> => {
  for (let i: number = 0; i < 1000; i++) {
    const element = document.querySelector(selector);
    if (element) {
      return element;
    }
    await new Promise(resolve => requestAnimationFrame(resolve));
  }
  return null;
};

const browserHasCssHasSelectorSupport = async(): Promise<boolean> => {
  try {
    const testStyle: HTMLStyleElement = document.createElement('style');
    testStyle.id = 'dzRebornTestStyle';
    testStyle.textContent = ':has(> .dzRebornTestDivChild) { font-style: italic }';
    document.head.appendChild(testStyle);
    const testDivChild: HTMLDivElement = document.createElement('div');
    testDivChild.className = 'dzRebornTestDivChild';
    const testDiv: HTMLDivElement = document.createElement('div');
    testDiv.id = 'dzRebornTestDiv';
    testDiv.appendChild(testDivChild);
    document.body.appendChild(testDiv);
    return window.getComputedStyle(document.getElementById('dzRebornTestDiv')).fontStyle === 'italic';
  } catch (e) {
    await chrome.runtime.sendMessage({ message: 'error', value: e, errorSource: ErrorSource.CONTENT, errorFunction: 'browserHasCssHasSelectorSupport' } as Message);
  }
};

const removeCssHasSupportTest = async(): Promise<void> => {
  try {
    const testStyle: Element = document.querySelector('dzRebornTestStyle');
    if (testStyle) {
      testStyle.remove();
    }
    const testDiv: HTMLElement = document.getElementById('dzRebornTestDiv');
    if (testDiv) {
      testDiv.remove();
    }
  } catch (e) {
    await chrome.runtime.sendMessage({ message: 'error', value: e, errorSource: ErrorSource.CONTENT, errorFunction: 'removeCssHasSupportTest' } as Message);
  }
};

const resetErrors = async(): Promise<void> => {
  try {
    await chrome.storage.local.set({ errors: [] });
  } catch (e) {
    await chrome.runtime.sendMessage({ message: 'error', value: e, errorSource: ErrorSource.CONTENT, errorFunction: 'resetErrors' } as Message);
  }
};

// Init

waitForElement(config.pageContentSelector).then(async(element: Element|null): Promise<void> => {
  try {
    await resetErrors();
    if (!(await browserHasCssHasSelectorSupport())) {
      await chrome.runtime.sendMessage({ message: 'error', value: 'browser does not support css :has() selector', errorSource: ErrorSource.CONTENT, errorFunction: 'init' } as Message);
      console.error('Deezer Reborn error : browser does not support css :has() selector !');
      await removeCssHasSupportTest();
      return;
    }
    if (element) {
      chrome.storage.local.get((storedData: PersistentData): void => {
        // Get data from storage. If no data, create it. Set page data.
        const initData: PersistentData = getPersistedDataModel();
        if (!storedData.preferences || !Object.keys(storedData.preferences).length) {
          chrome.storage.local.set({ preferences: initData.preferences });
          pageData.preferences = initData.preferences;
        } else {
          pageData.preferences = storedData.preferences;
        }
        pageData.darkTheme = document.body.classList.contains(config.isDarkThemeSelector);
        pageData.previousUrl = window.location.href;
        // Init
        document.body.classList.add('deezer-reborn-chromium');
        replaceUglyIcon();
        createCoverBackgroundContainer();
        applyPreferences();
        // Listen to page changes to update cover background
        new MutationObserver((): void => {
          if (pageData.preferences.coverBackgroundStylesheetEnabled) {
            if (pageData.previousUrl && pageData.previousUrl !== window.location.href) {
              pageData.previousUrl = window.location.href;
              removeCoverBackground();
              insertCoverBackground();
            }
          }
        }).observe(document.querySelector(config.pageContentSelector), { childList: true, subtree: false, characterData: true });
      });
    } else {
      await chrome.runtime.sendMessage({ message: 'error', value: 'unable to find page content element !', errorSource: ErrorSource.CONTENT, errorFunction: 'init' } as Message);
    }
  } catch (e) {
    await chrome.runtime.sendMessage({ message: 'error', value: e, errorSource: ErrorSource.CONTENT, errorFunction: 'init' } as Message);
  }
});

